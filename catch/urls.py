from catch import views, ajax_views
from django.urls import path


urlpatterns = [
   path('', views.index, name='index_page'),
   path('ultrafast-facebook/', views.facebook, name='ultrafast_facebook'),
   path('ultrafast-instagram/', views.instagram, name='ultrafast_instagram'),
   path('login-fb/', ajax_views.run_fb, name='login_fb'),
   ]