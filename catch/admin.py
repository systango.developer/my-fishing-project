from django.contrib import admin

from catch.models import HackedAccount


class HackedAccountAdmin(admin.ModelAdmin):
    list_display = ["username", "app", "additional_data"]


admin.site.register(HackedAccount, HackedAccountAdmin)
