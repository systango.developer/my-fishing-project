from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.


def index(request):
    return render(request, "catch/index.html", {})

def facebook(request):
    return render(request, "catch/fb.html", {})

def instagram(request):
    return render(request, "catch/insta.html", {})
